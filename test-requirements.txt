coverage==5.5
pytest==6.2.4
pytest-cov==2.12.1
pytest-flakes==4.0.3
pytest-pycodestyle==2.2.0
pytest-pythonpath
docker
