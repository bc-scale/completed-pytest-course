from scripts.chp2_tdd_pytest.video4_5_exceptions_challenge.mapmaker_solution import Point
import pytest


def test_make_one_point():
    p1 = Point("Oslo", 59.9139, 10.7523)
    assert p1.get_lat_long() == (59.9139, 10.7523)


def test_invalid_point_generation():
    with pytest.raises(ValueError) as exp:
        Point("Ottawa", 45.42472, -755.69500)
    assert str(exp.value) == "Invalid [latitude, longitude] combination."

    with pytest.raises(ValueError) as exp:
        Point(132812489127, 45.42472, -75.69500)
    assert str(exp.value) == "The city name provided isn't a string."
