from scripts.chp2_tdd_pytest.video2_assertions.mapmaker import Point


def test_make_one_point():
    pnt1 = Point("Oslo", 59.9139, 10.7523)
    assert pnt1.get_lat_long() == (59.9139, 10.7523)
